﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInternalForCIWithoutSpecflowRun
{
    public class Calculator
    {
        public double Add(double a, double b)
        {
            return InternalAdd(a, b);
        }
        
        internal double InternalAdd(double a, double b)
        {
            double result = a + b;
            return result;
        }
    }
}
