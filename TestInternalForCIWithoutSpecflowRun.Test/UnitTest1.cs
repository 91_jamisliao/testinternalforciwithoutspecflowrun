﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestInternalForCIWithoutSpecflowRun;

namespace TestInternalForCIWithoutSpecflowRun.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Calculator calculator = new Calculator();
            var result = calculator.InternalAdd(1, 2);
            Assert.AreEqual(result, 3);
        }
    }
}
